# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

# container_scanning:
#   variables:
#     DOCKER_IMAGE: ...
#     DOCKER_USER: ...
#     DOCKER_PASSWORD: ...
stages:
  - build
  - test
  - deploy

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Jobs/Build.gitlab-ci.yml
  - template: Jobs/Container-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml

sast:
  stage: test


code_quality:
  # per averli sia in JSON che html scommenta la riga seguente
  # extends: code_quality
  variables:
    REPORT_FORMAT: html
  artifacts:
    paths: [gl-code-quality-report.html]
    expire_in: 6 mos
  rules:
    - if: $CODE_QUALITY_DISABLED
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" # Run code quality job in merge request pipelines
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH      # Run code quality job in pipelines on the default branch (but not in other branch pipelines)
    - if: $CI_COMMIT_TAG                               # Run code quality job in pipelines for tags



release:
  stage: build
  image: golang:1.20
  script:
    GOOS=linux GOARCH=amd64 go build -o retryorchestrator
  artifacts:
    # instead of manually adding i.e. the built binaries, we can instead just
    # grab anything not tracked in Git
    untracked: true
    expire_in: 1 hour
  only:
    refs:
      - main
      - tags

deploy_tag:
  stage: deploy
  environment:
    name: qa
  tags:
    - boat-deploy
  script:
    - | 
      if [[ -z $SWARM_SERVICE_QA ]]; then
        echo "Error, missing SWARM_SERVICE_QA variable, cannot deploy"
        exit 1
      fi  
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag="latest"
        echo "Deploying image '${CI_REGISTRY_IMAGE}:${tag}' on default branch '$CI_DEFAULT_BRANCH'"
      fi  
      docker service update --image "$CI_REGISTRY_IMAGE:${tag}" "$SWARM_SERVICE_QA"
  only:
    - master
