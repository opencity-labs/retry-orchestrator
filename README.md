# RetryOrchestrator

## Contesto
Questo componente si occupa di rischedulare gli eventi per i relativi consumer quando vi è un errore temporaneo che ne impedisce il corretto processamento

## Cosa fa
Quando il servizio X fallisce nel processare un messaggio, immette il messaggio nel topic dei messaggi falliti (Failed Message Topic) \
Il RetryOrchestrator implementa il sistema di retry: legge dal FMT e in base alla politica di retry scelta, riproporrà tali messaggi al servizio X per un determinato numero di volte e dopo una determinata attesa in modo che il servizio X possa ritentarne il processamento.
### Politica di retry
Le seguenti variabili vengono utilizzate per stabilire la politica di retry. 

        KAFKA_DISPATCHER_POLICY:	2x1m,1x2m,3x3m
        KAFKA_RETRY_QUEUE_PREFIX: retryQueue_

KAFKA_DISPATCHER_POLICY prende un array di valori "numero-di-tentativi-X-tempo-di-attesa". Quindi nell'esempio  sopra esposto, ogni messaggio sarà trattato nel seguente modo: 
2 tentavivi da 1 minuto(2x1m), il terzo tentativo avverrà dopo 2 minuti(1x2m) e infine, ci saranno 3 tentativi da 3 minuti di attesa ciascuno. Dopo i tentatvi 4,5,6 da 3 min ciascuno(3x3m) il messaggio avrà raggunto il numero massimo di tentativi (2+1+3) e verrà aggiunto nella coda di retry. 

KAFKA_RETRY_QUEUE_PREFIX in combinazione con il tempi d'attesa della variabile KAFKA_DISPATCHER_POLICY definisce i nomi e il numero dei topic di attesa utilizzati dal servizio.
In questo esempio KAFKA_DISPATCHER_POLICY è un array di tre elementi, quindi il servizio si aspetta di trovare su kafka tre topic con i seguenti nomi: 
 retryQueue_1m, retryQueue_2m, retryQueue_3m


## Struttura del repository
Il repository ha come unico riferimento il branch main. \
La struttura di base riprende i principi del hexagonal architecture in cui:\
**main.go** - entrypoint del servizio. Qui vengono assemblate e avviate le strutture seguenti.\
**intenrnal/core/domain**: - contiene le entità utilizzate dalla business logic\
**internal/core/services**: - contiente la business logic\
**internal/core/ports**: - contiente le intefacce che descrivono come i driver e i driven interagiscono la busines logic\
**internal/adapters/drivers**: - contiene i driver, ovvero le tecnologie esposte dal servizio per poter essere richiamati dall'esterno \
**internal/adapters/repositories**: - contiene i driven, ovvero le tecnologie utilizzate dal servizio per poter accedere al mondo esterno \
**config/** - contiene la struttura atta alla configurazione del servizio: variabili d'ambiente, configurazione hardcoded\
**metrics/** - contiene la descrizione delle metriche Prometheus esposte dal servizio\
**sentryUtils/** - contiene la configurazione di Sentry per il monitoring degli errori\
**logger/** - contiene la configurazione del sistema di logs in formato in formato Apache\
**utils/** - contiene le utils del codice(funzioni accessorie..) \
Altri File: \
file di configurazione per la continuous integration:\ 
dockerFile, docker-compose.yml, docker-compose.ovverride.yml, gitlab-ci.yml, dockerignore, gitignore\

## Prerequisiti e dipendenze
tutte le librerie esterne utilizzate sono elencate nel file go.mod. La sola presenza di tale file e del suo attuale contenuto permette una gestione automatica delle dipendenze. \
È necessaria l'installazione di Docker e Docker Compose per il deploy automatico del servizio e di tutti gli applicativi accessori(kafka, kafka-ui, ksqldb-server, ksqldb-cli, zookeeper..) .. 

## Monitoring
il sistema usufruisce di Sentry per il monitoring degli errori, inoltre sono esposti i seguenti endpoint per il monitoring : \
- /status : endpoint per l'healtcheck. Resituisce 200
- /metrics : espone le metriche in formato Prometheus

### Configurazione variabili d'ambiente
| Nome                             | Default | Descrizione                                                             |
|----------------------------------|---------|-------------------------------------------------------------------------|
| KAFKA_SERVER                     | test    | indirizzo del broker kafka per connetersi al cluster                    |
| KAFKA_RETRY_QUEUE_TOPIC          | test    | nome del topic da cui ascolta il dispatcher                             |
| KAFKA_QUEQUE_CONSUMER_GROUP      | test    | consumer group                                                          |
| KAFKA_RETRY_QUEUE_TOPIC_LIST     | test    | lista dei topic di attesa da cui ascoltano i retryHandler               |
| KAFKA_DEAD_LETTER_QUEUE          | test    | topic in cui vanno a finire i messaggi che hanno esaurito i tentativi   |
| KAFKA_DISPATCHER_POLICY          | test    | politica di retry(spiegata nella sezione "Politica di retry")           | 
| ENVIRONMENT                      | local   | indica l'ambiente di sviluppo(locale, dev,prod...)                      |
| SERVER_ADDRESS_PORT              | test    | endpoint per il server http, utilizzato per l' heathcheck  e le metriche di prometheus             | 
| SENTRY_DSN                       | test    | url del dns di Sentry                                                   |
| SERVER_DEBUG                       | false    |mostra i log delle chiamate http ricevute                                                  |
| CACHE_EXPIRATION                       | 10m    | cache per il controllo del retry counter                                                 |
| CACHE_EVICTION                       | 10m    | cache per il controllo del retry counter                                                  |

## Come si usa
Aggiungere il file docker-compose.override.yml avente seguente struttura : 
```yaml
version: '3'

services:
  retryorchestrator:
    build:
      context: .
    environment:
      KAFKA_SERVER: <your-value>
      KAFKA_RETRY_QUEUE_TOPIC: <your-value>
      KAFKA_DISPATCHER_POLICY: <your-value>
      KAFKA_RETRY_QUEUE_PREFIX: <your-value>
      KAFKA_DEAD_LETTER_QUEUE: <your-value>
      KAFKA_RETRY_QUEUE_CONSUMER_GROUP: <your-value>
      ENVIRONMENT: <your-value>
      SERVER_ADDRESS_PORT: <your-value>
      SENTRY_DSN: "<your-value>"
    ports:
      - "9090:9090"

```

Da terminale: 
1. `git clone  git@gitlab.com:opencity-labs/retry-orchestrator.git`
2. `cd retry-orchestrator`
3. `docker-compose build`
4. `docker compose up -d`

## Testing
sono presenti unit test. per lanciarli, utilizzare il commando `go test .`

## Stadio di sviluppo

il software si trova in produzione. Verisone attuale 1.0.3

## Autori e Riconoscimenti

* Opencity Labs

## License

AGPL 3.0 only

## Contatti

support@opencitylabs.it