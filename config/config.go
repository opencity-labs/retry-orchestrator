package config

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/eko/gocache/lib/v4/cache"
	goCacheStore "github.com/eko/gocache/store/go_cache/v4"
	goCache "github.com/patrickmn/go-cache"
	"go.uber.org/zap"
)

type Config struct {
	logger *zap.Logger
	Kafka  struct {
		KafkaServer       []string
		RetryQueueTopic   string
		DLQTopic          string
		ConsumerGroup     string
		RetryTopicsList   []string
		RetryTopicsPrefix string
	}
	Policy struct {
		TopicSleepTimeMap  map[string]int
		RetryPolicy        []string
		TopicRetryAttempts []int
		TopicSleeps        []int
	}
	Sentry struct {
		Dsn string
	}
	App struct {
		Environment string
		Version     string
	}
	Server struct {
		AddressPort string
		Debug       bool
	}
	Cache struct {
		Eviction     time.Duration
		Expiration   time.Duration
		CounterCache *cache.Cache[*CacheCounter]
	}
}

func NewConfig(log *zap.Logger) *Config {

	c := &Config{
		logger: log,
	}
	c.Policy.TopicSleepTimeMap = make(map[string]int)
	c.init()
	c.initCounterCache()
	return c
}

func (c *Config) init() {
	//retryQueue topics configuration
	kafkaServer := c.getFromEnv("KAFKA_SERVER", "kafka:9092")
	c.Kafka.KafkaServer = strings.Split(kafkaServer, ",")
	c.Kafka.RetryQueueTopic = c.getFromEnv("KAFKA_RETRY_QUEUE_TOPIC", "test")
	c.Kafka.DLQTopic = c.getFromEnv("KAFKA_DEAD_LETTER_QUEUE", "test")
	c.Kafka.ConsumerGroup = c.getFromEnv("KAFKA_RETRY_QUEUE_CONSUMER_GROUP", "test")
	c.Kafka.RetryTopicsPrefix = c.getFromEnv("KAFKA_RETRY_QUEUE_PREFIX", "test")
	c.Server.AddressPort = c.getFromEnv("SERVER_ADDRESS_PORT", "test")
	c.Server.Debug = c.getBoolFromEnv("SERVER_DEBUG", false)
	cacheEviction := c.getFromEnv("CACHE_EVICTION", "10m")
	// Parse the duration string to time.Duration
	eviction, err := time.ParseDuration(cacheEviction)
	if err != nil {
		c.logger.Fatal("Error retrieving cacheEviction from env")
		return
	}
	c.Cache.Eviction = eviction
	cacheExpiration := c.getFromEnv("CACHE_EXPIRATION", "10m")
	expiration, err := time.ParseDuration(cacheExpiration)
	if err != nil {
		c.logger.Fatal("Error retrieving expiration from env")
		return
	}
	c.Cache.Expiration = expiration
	c.getRetrypolicy()
	c.BuildPolicyDataStructs()

	c.App.Environment = c.getFromEnv("ENVIRONMENT", "local")
	c.App.Version = VERSION

	c.Sentry.Dsn = c.getFromEnv("SENTRY_DSN", "test")
}

func (c *Config) getFromEnv(name string, defaultValue string) string {
	if val, ok := os.LookupEnv(name); ok {
		c.logger.Sugar().Debug("found env var : ", name, ": ", val)
		return val
	}
	c.logger.Sugar().Debug("using default value for environment var: ", name)
	return defaultValue
}

func (c *Config) getRetrypolicy() {
	policy := c.getFromEnv("KAFKA_DISPATCHER_POLICY", "test")
	c.Policy.RetryPolicy = strings.Split(policy, ",")
}

func (c *Config) BuildPolicyDataStructs() {
	topicListIndex := 0
	for _, values := range c.Policy.RetryPolicy {
		infoPolicy := strings.Split(values, "x")
		if len(infoPolicy) == 2 {

			c.Kafka.RetryTopicsList = append(c.Kafka.RetryTopicsList, c.Kafka.RetryTopicsPrefix+infoPolicy[1]) /* c.Kafka.RetryTopicsPrefix + infoPolicy[1] */
			c.buildTopicSleepTimeMap(infoPolicy[1], topicListIndex)
			c.buildTopicRetryAttemptsSlice(infoPolicy[0])
		}
		topicListIndex++
	}
}

func (c *Config) extractDigits(input string) (int, error) {
	re := regexp.MustCompile(`^(\d+)m$`)
	match := re.FindStringSubmatch(input)

	if len(match) != 2 {
		return 0, fmt.Errorf("invalid input format")
	}

	digitStr := match[1]
	digit, err := strconv.Atoi(digitStr)
	if err != nil {
		return 0, fmt.Errorf("failed to convert digit to integer")
	}

	return digit, nil
}

func (c *Config) buildTopicRetryAttemptsSlice(sleepTimeStr string) {
	retryAttempts, err := strconv.Atoi(sleepTimeStr)
	if err != nil {
		c.logger.Fatal("Error during conversion from string to int")
	}
	c.Policy.TopicRetryAttempts = append(c.Policy.TopicRetryAttempts, retryAttempts)
}

func (c *Config) buildTopicSleepTimeMap(sleepTimeStr string, topicListIndex int) {
	sleepTime, err := c.extractDigits(sleepTimeStr)
	if err != nil {
		c.logger.Fatal("Error extracting sleep time")
	}
	c.Policy.TopicSleepTimeMap[c.Kafka.RetryTopicsList[topicListIndex]] = sleepTime
}

func (c *Config) getBoolFromEnv(name string, defaultValue bool) bool {

	if val, ok := os.LookupEnv(name); ok {
		// Attempt to parse the environment variable as a boolean
		boolValue, err := strconv.ParseBool(val)
		if err != nil {
			// Log an error if parsing fails and return the default value
			c.logger.Sugar().Errorf("Error parsing boolean value for environment var %s: %v", name, err)
			return defaultValue
		}
		return boolValue
	}

	c.logger.Sugar().Debug("Using default value for environment var: ", name)
	return defaultValue
}

func (c *Config) initCounterCache() {
	goCache := goCache.New(c.Cache.Expiration, c.Cache.Eviction)
	goCacheStore := goCacheStore.NewGoCache(goCache)

	c.Cache.CounterCache = cache.New[*CacheCounter](goCacheStore)
}

type CacheCounter struct {
	EventId      string  `json:"event_id"`
	RetryCounter float64 `json:"retry_counter"`
}
