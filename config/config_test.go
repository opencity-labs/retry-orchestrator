package config

import (
	"testing"

	"go.uber.org/zap"
	"opencitylabs.it/retryOrchestrator/logger"
)

func setup(t *testing.T) (*zap.Logger, *Config) {
	// Logger initialization
	logger, err := logger.GetLoggerConfig().Build()
	if err != nil {
		logger = zap.NewNop()
	}
	defer logger.Sync()

	// Config initialization
	config := NewConfig(logger)

	return logger, config
}

/* if the message comes for the first time, it will be without fields. In this case the retrycounter should be set to 1 */
func TestBuildPolicyDataStructs(t *testing.T) {

	logger, config := setup(t)
	defer logger.Sync()

	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.Kafka.DLQTopic = "retryDLQ"
	config.BuildPolicyDataStructs()
	//test TopicSleepTimeMap
	if config.Policy.TopicSleepTimeMap["retryQueue_2m"] != 2 {
		t.Errorf("key \"retryQueue_2m\" in TopicSleepTimeMap should have value \"2\". current value: %v", config.Policy.TopicSleepTimeMap["retryQueue_2m"])
	}
	if config.Policy.TopicSleepTimeMap["retryQueue_5m"] != 5 {
		t.Errorf("key \"retryQueue_5m\" in TopicSleepTimeMap should have value \"5\". current value: %v", config.Policy.TopicSleepTimeMap["retryQueue_5m"])
	}
	if config.Policy.TopicSleepTimeMap["retryQueue_20m"] != 20 {
		t.Errorf("key \"retryQueue_20m\" in TopicSleepTimeMap should have value \"20\". current value: %v", config.Policy.TopicSleepTimeMap["retryQueue_20m"])
	}

	//test RetryTopicsList
	if config.Kafka.RetryTopicsList[0] != "retryQueue_2m" {
		t.Errorf("key \"position 0\" in RetryTopicsList should have value \"retryQueue_2m\". current value: %v", config.Kafka.RetryTopicsList[0])
	}
	if config.Kafka.RetryTopicsList[1] != "retryQueue_5m" {
		t.Errorf("key \"position 1\" in RetryTopicsList should have value \"retryQueue_5m\". current value: %v", config.Kafka.RetryTopicsList[1])
	}
	if config.Kafka.RetryTopicsList[2] != "retryQueue_20m" {
		t.Errorf("key \"position 2\" in RetryTopicsList should have value \"retryQueue_20m\". current value: %v", config.Kafka.RetryTopicsList[2])
	}

	// test TopicRetryAttempts
	if config.Policy.TopicRetryAttempts[0] != 2 {
		t.Errorf("key \"position 0\" in TopicRetryAttempts should have value \"2\". current value: %v", config.Policy.TopicRetryAttempts[0])
	}
	if config.Policy.TopicRetryAttempts[1] != 10 {
		t.Errorf("key \"position 1\" in TopicRetryAttempts should have value \"10\". current value: %v", config.Policy.TopicRetryAttempts[1])
	}
	if config.Policy.TopicRetryAttempts[2] != 4 {
		t.Errorf("key \"position 2\" in TopicRetryAttempts should have value \"4\". current value: %v", config.Policy.TopicRetryAttempts[2])
	}
}

func TestBuildPolicyDataStructsTwo(t *testing.T) {

	logger, config := setup(t)
	defer logger.Sync()

	config.Policy.RetryPolicy = []string{"3x1m", "2x3m", "4x15m", "5x30m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.Kafka.DLQTopic = "retryDLQ"
	config.BuildPolicyDataStructs()

	logger.Sugar().Debug("config.Policy.TopicSleepTimeMap: ", config.Policy.TopicSleepTimeMap)
	//TopicSleepTimeMap test
	if config.Policy.TopicSleepTimeMap["retryQueue_1m"] != 1 {
		t.Errorf("key \"retryQueue_1m\" in TopicSleepTimeMap should have value \"1\". current value: %v", config.Policy.TopicSleepTimeMap["retryQueue_1m"])
	}
	if config.Policy.TopicSleepTimeMap["retryQueue_3m"] != 3 {
		t.Errorf("key \"retryQueue_3m\" in TopicSleepTimeMap should have value \"3\". current value: %v", config.Policy.TopicSleepTimeMap["retryQueue_3m"])
	}
	if config.Policy.TopicSleepTimeMap["retryQueue_15m"] != 15 {
		t.Errorf("key \"retryQueue_15m\" in TopicSleepTimeMap should have value \"15\". current value: %v", config.Policy.TopicSleepTimeMap["retryQueue_15m"])
	}
	if config.Policy.TopicSleepTimeMap["retryQueue_30m"] != 30 {
		t.Errorf("key \"retryQueue_30m\" in TopicSleepTimeMap should have value \"30\". current value: %v", config.Policy.TopicSleepTimeMap["retryQueue_30m"])
	}
}

func TestExtractDigits(t *testing.T) {
	logger, config := setup(t)
	defer logger.Sync()
	// Test case 1: Valid input format
	input1 := "42m"
	expectedResult1 := 42
	digit1, err1 := config.extractDigits(input1)
	if err1 != nil {
		t.Errorf("Expected no error, got: %v", err1)
	}
	if digit1 != expectedResult1 {
		t.Errorf("Expected digit %d, got: %d", expectedResult1, digit1)
	}

	// Test case 2: Invalid input format
	input2 := "abc"
	expectedError2 := "invalid input format"
	_, err2 := config.extractDigits(input2)
	if err2 == nil {
		t.Error("Expected an error, got nil")
	}
	if err2.Error() != expectedError2 {
		t.Errorf("Expected error '%s', got: '%s'", expectedError2, err2.Error())
	}

	input3 := "1320m"
	expectedResult3 := 1320
	digit3, err3 := config.extractDigits(input3)
	if err3 != nil {
		t.Error("Expected an error, got nil")
	}
	if digit3 != expectedResult3 {
		t.Errorf("Expected digit %d, got: %d", expectedResult3, digit3)
	}
}
