package constants

const RETRY_OBJECT_NAME = "retry_meta"
const RETRY_COUNTER = "retry_counter"
const NEXT_ATTEMPT = "next_attempt_at"
const ORIGINAL_TOPIC = "original_topic"
const DATA_TIME_LAYOUT = "2006-01-02 15:04:05.999999999 -0700 MST"
const EVENT_ID = "id"
const APP_ID = "app_id"
