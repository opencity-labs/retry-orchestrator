package httpserver

import (
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"opencitylabs.it/retryOrchestrator/config"
)

type server struct {
	log  *zap.Logger
	conf config.Config
	srv  *gin.Engine
}

func NewHttpServer(l *zap.Logger, c config.Config) *server {
	s := &server{
		log:  l,
		conf: c,
	}
	gin.SetMode(gin.ReleaseMode)
	if !c.Server.Debug {
		gin.DefaultWriter = io.Discard
	}
	s.srv = gin.Default()
	s.srv.ForwardedByClientIP = true
	s.srv.SetTrustedProxies([]string{s.conf.Server.AddressPort})
	s.initRoutes()

	return s
}

func (r *server) initRoutes() {
	r.srv.GET("/status", r.healthCheckHandler)
	r.srv.GET("/metrics", gin.WrapH(promhttp.Handler()))
}

func (r *server) Run() {
	r.srv.Run(r.conf.Server.AddressPort)
}

func (r *server) healthCheckHandler(c *gin.Context) {
	c.String(http.StatusOK, "Healthy")
}
