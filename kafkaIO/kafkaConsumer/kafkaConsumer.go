package kafkaconsumer

import (
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/retryOrchestrator/config"
)

type KafkaConsumer struct {
	KafkaReader *kafka.Reader
	logger      *zap.Logger
	config      *config.Config
}

// todo passare sentry
func NewKafkaConsumer(log *zap.Logger, conf *config.Config, kafkaServer []string, kafkaTopic, consumerGroup string) *KafkaConsumer {
	ec := &KafkaConsumer{
		logger: log,
		config: conf,
	}
	ec.KafkaReader = ec.getKafkaReader(kafkaServer, kafkaTopic, consumerGroup)
	return ec
}

func (ec *KafkaConsumer) getKafkaReader(kafkaServer []string, kafkaTopic, consumerGroup string) *kafka.Reader {
	ec.logger.Sugar().Debug("Connecting to topic: ", kafkaTopic, " from kafka server ", kafkaServer)
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: kafkaServer,
		Topic:   kafkaTopic,
		GroupID: consumerGroup,
		MaxWait: 3 * time.Second,
		//PartitionWatchInterval: 5 * time.Second,
		//WatchPartitionChanges:  true,
		//StartOffset:            kafka.LastOffset,
		//ReadBackoffMax:         86400 * 10 * time.Second,
		//Logger:                 log.Default(),
		//OffsetOutOfRangeError:  true,
	})
	return r
}

func (ec *KafkaConsumer) CloseKafkaReader() {
	err := ec.KafkaReader.Close()
	if err != nil {
		ec.logger.Error("Error closing consumer: ", zap.Error(err))
		sentry.CaptureException(err)
		return
	}
	ec.logger.Debug("Consumer closed")
}
