package kafkamessagedispatcher

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"go.uber.org/zap"
	"opencitylabs.it/retryOrchestrator/config"
	"opencitylabs.it/retryOrchestrator/constants"
	"opencitylabs.it/retryOrchestrator/logger"
)

func setup(t *testing.T, inputMessage string) (*zap.Logger, *config.Config, map[string]interface{}, context.Context) {
	// Logger initialization
	logger, err := logger.GetLoggerConfig().Build()
	if err != nil {
		logger = zap.NewNop()
	}
	defer logger.Sync()

	// Config initialization
	config := config.NewConfig(logger)

	// Message map initialization
	var messageMap map[string]interface{}
	err = json.Unmarshal([]byte(inputMessage), &messageMap)
	if err != nil {
		logger.Debug("error unmarshalling message into map")
		t.Fatal(err)
	}
	ctx, _ := context.WithCancel(context.Background())
	return logger, config, messageMap, ctx
}

/* if the message comes for the first time, it will be without fields. In this case the retrycounter should be set to 1 */
func TestGetRetryCounterValue(t *testing.T) {
	expectedRetryCounterValue := 1.0
	logger, config, messageMap, ctx := setup(t, INPUT_MESSAGE_WITHOUT_RETRY_FIELDS)
	defer logger.Sync()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	retryMetaObj, _ := messageMap[constants.RETRY_OBJECT_NAME].(map[string]interface{})
	currentValue := dispatcher.getRetryCounterValue(&retryMetaObj)

	if currentValue != expectedRetryCounterValue {
		t.Errorf("field retry_meta wrong. current value: %v", currentValue)
	}
}

/* if the message doesn't come for the first time, it should contains the retryCounter Field. In this case the retrycounter should be incremented by one */

func TestGetRetryCounterValueTwo(t *testing.T) {
	expectedRetryCounterValue := 7.0
	logger, config, messageMap, ctx := setup(t, INPUT_MESSAGE_WITH_RETRY_FIELDS)
	defer logger.Sync()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	retryMetaObj, _ := messageMap[constants.RETRY_OBJECT_NAME].(map[string]interface{})
	currentValue := dispatcher.getRetryCounterValue(&retryMetaObj)

	if currentValue != expectedRetryCounterValue {
		t.Errorf("field retry_meta wrong. current value: %v", currentValue)
	}
}

/* if the retry counter meta field contains wrong value, it shoud return -1-0 */
func TestGetRetryCounterValueThree(t *testing.T) {
	expectedRetryCounterValue := -1.0
	logger, config, messageMap, ctx := setup(t, INPUT_MESSAGE_WITH_WRONG_RETRY_FIELDS)
	defer logger.Sync()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	retryMetaObj, _ := messageMap[constants.RETRY_OBJECT_NAME].(map[string]interface{})
	currentValue := dispatcher.getRetryCounterValue(&retryMetaObj)

	if currentValue != expectedRetryCounterValue {
		t.Errorf("field retry_meta wrong. current value: %v", currentValue)
	}
}

func TestGetRetryCounterValueFour(t *testing.T) {
	expectedRetryCounterValue := 4
	logger, config, messageMap, ctx := setup(t, INPUT_MESSAGE_WITH_RETRY_FIELDS_AND_ATTACHMENTS)
	defer logger.Sync()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	retryMetaObj, _ := messageMap[constants.RETRY_OBJECT_NAME].(map[string]interface{})
	currentValue := dispatcher.getRetryCounterValue(&retryMetaObj)

	if currentValue != float64(expectedRetryCounterValue) {
		t.Errorf("field retry_meta wrong. current value: %v", currentValue)
	}
}

func TestRetrieveDestinationTopic(t *testing.T) {
	expectedRetryCounterValue := "retryDLQ"
	logger, config, _, ctx := setup(t, INPUT_MESSAGE_WITHOUT_RETRY_FIELDS)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	currentValue := dispatcher.RetrieveDestinationTopic(137)
	logger.Sugar().Debug("current value : ", currentValue)
	if currentValue != expectedRetryCounterValue {
		t.Errorf("field retry_meta wrong. current value: %v", currentValue)
	}
}

func TestRetrieveDestinationTopicTwo(t *testing.T) {

	expectedRetryCounterValue := "retryQueue_5m"
	logger, config, _, ctx := setup(t, INPUT_MESSAGE_WITHOUT_RETRY_FIELDS)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	currentValue := dispatcher.RetrieveDestinationTopic(7)
	logger.Sugar().Debug("current value : ", currentValue)
	if currentValue != expectedRetryCounterValue {
		t.Errorf("field retry_meta wrong. current value: %v", currentValue)
	}
}

func TestRetrieveDestinationTopicThree(t *testing.T) {

	expectedRetryCounterValue := "retryQueue_2m"
	logger, config, _, ctx := setup(t, INPUT_MESSAGE_WITHOUT_RETRY_FIELDS)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	currentValue := dispatcher.RetrieveDestinationTopic(1)
	logger.Sugar().Debug("current value : ", currentValue)
	if currentValue != expectedRetryCounterValue {
		t.Errorf("field retry_meta wrong. current value: %v", currentValue)
	}
}

func TestRetrieveDestinationTopicFour(t *testing.T) {

	expectedRetryCounterValue := "retryQueue_20m"
	logger, config, _, ctx := setup(t, INPUT_MESSAGE_WITHOUT_RETRY_FIELDS)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	currentValue := dispatcher.RetrieveDestinationTopic(13)
	logger.Sugar().Debug("current value : ", currentValue)
	if currentValue != expectedRetryCounterValue {
		t.Errorf("field retry_meta wrong. current value: %v", currentValue)
	}
}

func TestGetNextRetryAttemptFiveMin(t *testing.T) {
	expectedMinSleepTime := 4.0
	expectedMaxSleepTime := 5.0

	logger, config, _, ctx := setup(t, INPUT_MESSAGE_WITHOUT_RETRY_FIELDS)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	nextRetryAttemp := dispatcher.getNextRetryAttempt("retryQueue_5m")
	logger.Sugar().Debug("current value : ", nextRetryAttemp)
	expectedSleepTime := time.Since(nextRetryAttemp).Minutes()
	logger.Sugar().Debug("duration value : ", expectedSleepTime)
	expectedSleepTime = expectedSleepTime * -1
	if !(expectedSleepTime >= expectedMinSleepTime && expectedSleepTime <= expectedMaxSleepTime) {
		t.Errorf("field retry_meta wrong. current value: %v", expectedSleepTime)
	}
}

func TestGetNextRetryAttemptTwoMin(t *testing.T) {
	expectedMinSleepTime := 1.0
	expectedMaxSleepTime := 2.0

	logger, config, _, ctx := setup(t, INPUT_MESSAGE_WITHOUT_RETRY_FIELDS)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	nextRetryAttemp := dispatcher.getNextRetryAttempt("retryQueue_2m")
	logger.Sugar().Debug("current value : ", nextRetryAttemp)
	expectedSleepTime := time.Since(nextRetryAttemp).Minutes()
	logger.Sugar().Debug("duration value : ", expectedSleepTime)
	expectedSleepTime = expectedSleepTime * -1
	if !(expectedSleepTime >= expectedMinSleepTime && expectedSleepTime <= expectedMaxSleepTime) {
		t.Errorf("field retry_meta wrong. current value: %v", expectedSleepTime)
	}
}

func TestGetNextRetryAttemptTwentyMin(t *testing.T) {
	expectedMinSleepTime := 19.0
	expectedMaxSleepTime := 20.0

	logger, config, _, ctx := setup(t, INPUT_MESSAGE_WITHOUT_RETRY_FIELDS)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "5x12m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	nextRetryAttemp := dispatcher.getNextRetryAttempt("retryQueue_20m")
	logger.Sugar().Debug("current value : ", nextRetryAttemp)
	expectedSleepTime := time.Since(nextRetryAttemp).Minutes()
	logger.Sugar().Debug("duration value : ", expectedSleepTime)
	expectedSleepTime = expectedSleepTime * -1
	if !(expectedSleepTime >= expectedMinSleepTime && expectedSleepTime <= expectedMaxSleepTime) {
		t.Errorf("field retry_meta wrong. current value: %v", expectedSleepTime)
	}
}

func TestSetRetryFieldsAndDestinationTopic(t *testing.T) {
	expectedRetryCounterValue := 1.0
	expectedDestinationTopic := "retryQueue_2m"
	logger, config, messageMap, ctx := setup(t, INPUT_MESSAGE_WITHOUT_RETRY_FIELDS)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "5x12m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	dispatcher.setRetryFieldsAndDestinationTopic(&messageMap)
	retryMetaObj, isPresent := messageMap[constants.RETRY_OBJECT_NAME].(map[string]interface{})

	//checking results
	if !isPresent {
		t.Errorf("field retry_meta is missing!")
	}
	if retryMetaObj["retry_counter"] != expectedRetryCounterValue {
		t.Errorf("field retry_counter present but value not zero. current value: %v", retryMetaObj["retry_counter"])
	}
	if retryMetaObj["original_topic"] != "topic_name" {
		t.Errorf("field original_topic present but value not as expected. current value: %v", retryMetaObj["original_topic"])
	}
	if retryMetaObj["next_attempt_at"] == "" {
		t.Errorf("field next_attempt_at present but value not as expected. current value: %v", retryMetaObj["next_attempt_at"])
	}
	if dispatcher.GetDestinationTopic() != expectedDestinationTopic {
		t.Errorf("expected destination topic: "+expectedDestinationTopic+" but got %v", dispatcher.GetDestinationTopic())
	}
}

func TestSetRetryFieldsAndDestinationTopiTwo(t *testing.T) {
	expectedRetryCounterValue := 7.0
	expectedOriginalTopic := "topic_name"
	expectedDestinationTopic := "retryQueue_5m"
	logger, config, messageMap, ctx := setup(t, INPUT_MESSAGE_WITH_RETRY_FIELDS)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "5x12m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	dispatcher.setRetryFieldsAndDestinationTopic(&messageMap)
	retryMetaObj, isPresent := messageMap[constants.RETRY_OBJECT_NAME].(map[string]interface{})

	//checking results
	if !isPresent {
		t.Errorf("field retry_meta is missing!")
	}
	if retryMetaObj["retry_counter"] != expectedRetryCounterValue {
		t.Errorf("field retry_counter present but value not zero. current value: %v", retryMetaObj["retry_counter"])
	}
	if retryMetaObj["original_topic"] != expectedOriginalTopic {
		t.Errorf("field original_topic present but value not as expected. current value: %v", retryMetaObj["original_topic"])
	}
	if retryMetaObj["next_attempt_at"] == "" {
		t.Errorf("field next_attempt_at present but value not as expected. current value: %v", retryMetaObj["next_attempt_at"])
	}
	if dispatcher.GetDestinationTopic() != expectedDestinationTopic {
		t.Errorf("expected destination topic: "+expectedDestinationTopic+" but got %v", dispatcher.GetDestinationTopic())
	}
}

func TestGetDestinationTopic(t *testing.T) {
	expectedRetryCounterValue := 22.0
	expectedDestinationTopic := "retryDLQ"
	logger, config, messageMap, ctx := setup(t, INPUT_MESSAGE_WITH_RETRY_FIELDS_MAX_ATEMPT)
	defer logger.Sync()

	config.Kafka.DLQTopic = "retryDLQ"
	config.Policy.RetryPolicy = []string{"2x2m", "10x5m", "5x12m", "4x20m"}
	config.Kafka.RetryTopicsPrefix = "retryQueue_"
	config.BuildPolicyDataStructs()

	dispatcher := NewKafkaMessageDispatcher(logger, *config, ctx)
	dispatcher.setRetryFieldsAndDestinationTopic(&messageMap)
	retryMetaObj, isPresent := messageMap[constants.RETRY_OBJECT_NAME].(map[string]interface{})

	//checking results
	if !isPresent {
		t.Errorf("field retry_meta is missing!")
	}
	if retryMetaObj["retry_counter"] != expectedRetryCounterValue {
		t.Errorf("current value: %v", retryMetaObj["retry_counter"])
	}
	if retryMetaObj["original_topic"] != "topic_name" {
		t.Errorf("field original_topic present but value not as expected. current value: %v", retryMetaObj["original_topic"])
	}
	if retryMetaObj["next_attempt_at"] == "" {
		t.Errorf("field next_attempt_at present but value not as expected. current value: %v", retryMetaObj["next_attempt_at"])
	}
	if dispatcher.GetDestinationTopic() != expectedDestinationTopic {
		t.Errorf("expected destination topic: "+expectedDestinationTopic+" but got %v", dispatcher.GetDestinationTopic())
	}
}
