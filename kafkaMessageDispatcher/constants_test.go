package kafkamessagedispatcher

const INPUT_MESSAGE_WITHOUT_RETRY_FIELDS = `{
	"AccountId": "0015E00003AcX8nQAF",
	"Categoria_del_Cittadino__r": {
	  "Name": "Anagrafe, elettorale e cimiteri"
	},
	"ContactId": "123",
	"Data_Apertura_Case__c": "2023-05-12T12:13:09+02:00",
	"Description": "www2",
	"Indirizzo__City__s": "",
	"Indirizzo__Latitude__s": 44.4124089,
	"Indirizzo__Longitude__s": 8.9282527,
	"Indirizzo__PostalCode__s": "16100",
	"Indirizzo__Street__s": "Via del Campo",
	"Municipio__c": "01",
	"Origin": "SegnalaCi",
	"Privacy__c": true,
	"Riepilogo_Case_Migrato__c": "",
	"SegnalaCiId__C": "456",
	"Subject": "www2",
	"Type": "segnalazione",
	   "retry_meta":
	 {
	   "original_topic": "topic_name"
	 }
  }`

const INPUT_MESSAGE_WITH_RETRY_FIELDS = `{
	"AccountId": "0015E00003AcX8nQAF",
	"Categoria_del_Cittadino__r": {
	  "Name": "Anagrafe, elettorale e cimiteri"
	},
	"ContactId": "123",
	"Data_Apertura_Case__c": "2023-05-12T12:13:09+02:00",
	"Description": "www2",
	"Indirizzo__City__s": "",
	"Indirizzo__Latitude__s": 44.4124089,
	"Indirizzo__Longitude__s": 8.9282527,
	"Indirizzo__PostalCode__s": "16100",
	"Indirizzo__Street__s": "Via del Campo",
	"Municipio__c": "01",
	"Origin": "SegnalaCi",
	"Privacy__c": true,
	"Riepilogo_Case_Migrato__c": "",
	"SegnalaCiId__C": "456",
	"Subject": "www2",
	"Type": "segnalazione",
	   "retry_meta":
	 {
	   "original_topic": "topic_name",
	   "retry_counter": 6,
	   "next_attempt_at": "2023-05-12T12:16:15+02:00"
	 }
  }`

const INPUT_MESSAGE_WITH_RETRY_FIELDS_AND_ATTACHMENTS = `{
	"allowed_readers": null,
	"app_id": "retry-orchestrator:1.0.10",
	"attachments": [
		{
			"description": "Allegato richiesta integrazione",
			"filename": "mypay_avviso_80034840167_00000000000068537.pdf",
			"md5": null,
			"mime_type": "application/pdf",
			"name": "mypay_avviso_80034840167_00000000000068537.pdf",
			"url": "https://servizi.comune-qa.bugliano.pi.it/lang/api/applications/03710227-d057-47f8-ba19-a846a693791c/attachments/37e00921-a562-40dd-8819-d0220c52315d?version=1"
		}
	],
	"author": [
		{
			"email": "mouslim97@gmail.com",
			"family_name": "Fatnassi",
			"name": "Mouslim",
			"postal_code": "36061",
			"role": "receiver",
			"street_name": "Via Ca' Baroncello,",
			"tax_identification_number": "FTNMLM97H16C111O",
			"type": "human"
		}
	],
	"business_events": null,
	"created_at": "2024-06-04T17:44:00+02:00",
	"description": "Protocollazione esterna [INSIEL REST] Mouslim Fatnassi 537f1b20-2a9e-4bb6-8d52-96f86b8c40db - 03710227-d057-47f8-ba19-a846a693791c",
	"event_created_at": "2024-06-04T17:46:37+06:00",
	"event_id": "160804af-943d-48ff-91c6-dbce943265bd",
	"event_version": 1,
	"folder": {
		"id": null,
		"title": "Protocollazione esterna [INSIEL REST]-Mouslim Fatnassi FTNMLM97H16C111O"
	},
	"id": "0af2dbb5-1ed0-4654-9040-1bba9a9ed5cd",
	"image_gallery": null,
	"life_events": null,
	"main_document": {
		"description": "Richiesta integrazione: 6371dd36-23f2-4071-8691-b4379db42178 2024-06-04 05:44",
		"filename": "665f36406ff38.pdf",
		"md5": null,
		"mime_type": "application/pdf",
		"name": "richiesta-integrazione-6371dd36-23f2-4071-8691-b4379db42178-202406040544.pdf",
		"url": "https://servizi.comune-qa.bugliano.pi.it/lang/api/applications/03710227-d057-47f8-ba19-a846a693791c/attachments/6371dd36-23f2-4071-8691-b4379db42178?version=1"
	},
	"normative_requirements": null,
	"owner_id": "537f1b20-2a9e-4bb6-8d52-96f86b8c40db",
	"recipient_type": "user",
	"registration_data": {
		"date": "2024-06-04T17:45:36+02:00",
		"document_number": null,
		"transmission_type": "Outbound"
	},
	"related_documents": null,
	"related_public_services": null,
	"remote_collection": {
		"id": "c0579e30-ca07-4a45-8b3c-b80132903219",
		"type": "service"
	},
	"remote_id": "03710227-d057-47f8-ba19-a846a693791c",
	"retry_meta": {
		"attachments": [
			{
				"id": "11412616484855940347",
				"inviaIOP": true,
				"nome": "richiesta-integrazione-6371dd36-23f2-4071-8691-b4379db42178-202406040544.pdf",
				"primario": true,
				"url": "https://servizi.comune-qa.bugliano.pi.it/lang/api/applications/03710227-d057-47f8-ba19-a846a693791c/attachments/6371dd36-23f2-4071-8691-b4379db42178?version=1"
			},
			{
				"id": "11095477605077750645",
				"inviaIOP": true,
				"nome": "mypay_avviso_80034840167_00000000000068537.pdf",
				"primario": false,
				"url": "https://servizi.comune-qa.bugliano.pi.it/lang/api/applications/03710227-d057-47f8-ba19-a846a693791c/attachments/37e00921-a562-40dd-8819-d0220c52315d?version=1"
			}
		],
		"next_attempt_at": "2024-06-04 15:46:36 +0000 UTC",
		"original_topic": "documents",
		"retryStatus": "NOT_PROCESSED",
		"retry_counter": 3
	},
	"short_description": "Protocollazione esterna [INSIEL REST] Mouslim Fatnassi 537f1b20-2a9e-4bb6-8d52-96f86b8c40db - 03710227-d057-47f8-ba19-a846a693791c",
	"source_type": "tenant",
	"tenant_id": "60e35f02-1509-408c-b101-3b1a28109329",
	"title": "Richiesta integrazione: Protocollazione esterna [INSIEL REST] Mouslim Fatnassi 537f1b20-2a9e-4bb6-8d52-96f86b8c40db - 03710227-d057-47f8-ba19-a846a693791c",
	"topics": null,
	"type": "integration-request",
	"updated_at": "2024-06-04T17:46:37+06:00"
}`

const INPUT_MESSAGE_WITH_WRONG_RETRY_FIELDS = `{
	"AccountId": "0015E00003AcX8nQAF",
	"Categoria_del_Cittadino__r": {
	  "Name": "Anagrafe, elettorale e cimiteri"
	},
	"ContactId": "123",
	"Data_Apertura_Case__c": "2023-05-12T12:13:09+02:00",
	"Description": "www2",
	"Indirizzo__City__s": "",
	"Indirizzo__Latitude__s": 44.4124089,
	"Indirizzo__Longitude__s": 8.9282527,
	"Indirizzo__PostalCode__s": "16100",
	"Indirizzo__Street__s": "Via del Campo",
	"Municipio__c": "01",
	"Origin": "SegnalaCi",
	"Privacy__c": true,
	"Riepilogo_Case_Migrato__c": "",
	"SegnalaCiId__C": "456",
	"Subject": "www2",
	"id": "dasdf4464s",
	"Type": "segnalazione",
	   "retry_meta":
	 {
	   "original_topic": "topic_name",
	   "retry_counter": "WRONG TYPE",
	   "next_attempt_at": "2023-05-12T12:16:15+02:00"
	 }
  }`

const EXPECTED_RETRY_COUNTER_ZERO = 0

const INPUT_MESSAGE_WITH_RETRY_FIELDS_MAX_ATEMPT = `{
	"AccountId": "0015E00003AcX8nQAF",
	"Categoria_del_Cittadino__r": {
	  "Name": "Anagrafe, elettorale e cimiteri"
	},
	"ContactId": "123",
	"Data_Apertura_Case__c": "2023-05-12T12:13:09+02:00",
	"Description": "www2",
	"Indirizzo__City__s": "",
	"Indirizzo__Latitude__s": 44.4124089,
	"Indirizzo__Longitude__s": 8.9282527,
	"Indirizzo__PostalCode__s": "16100",
	"Indirizzo__Street__s": "Via del Campo",
	"Municipio__c": "01",
	"Origin": "SegnalaCi",
	"Privacy__c": true,
	"Riepilogo_Case_Migrato__c": "",
	"SegnalaCiId__C": "456",
	"Subject": "www2",
	"Type": "segnalazione",
	   "retry_meta":
	 {
	   "original_topic": "topic_name",
	   "retry_counter": 21,
	   "next_attempt_at": "2023-05-12T12:16:15+02:00"
	 }
  }`
