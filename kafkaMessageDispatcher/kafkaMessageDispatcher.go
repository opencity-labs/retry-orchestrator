package kafkamessagedispatcher

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"go.uber.org/zap"
	"opencitylabs.it/retryOrchestrator/config"
	"opencitylabs.it/retryOrchestrator/constants"
	"opencitylabs.it/retryOrchestrator/utils"
)

type IKafkaMessageDispatcher interface {
	GetProcessedMessageAndDestinationTopic(m []byte) ([]byte, string, error)
	setRetryFieldsAndDestinationTopic(message *map[string]interface{}) error
	getRetryCounterValue(retryMeta *map[string]interface{}) float64
	RetrieveDestinationTopic(retryCounter int) string
	getNextRetryAttempt(destinationTopic string) time.Time
	GetDestinationTopic() string
	GetSourceTopic() string
	GetEventId() string
	GetRetryCounter() float64
	GetNextRetryAttempt() string
	GetAppId() string
}

type kafkaMessageDispatcher struct {
	log               *zap.Logger
	conf              config.Config
	DestinationTopic  string
	RetryCounterValue float64
	SourceTopic       string
	EventId           string
	NextRetryAttempt  string
	AppId             string
	ctx               context.Context
}

func NewKafkaMessageDispatcher(l *zap.Logger, c config.Config, ctx context.Context) IKafkaMessageDispatcher {
	return &kafkaMessageDispatcher{
		log:  l,
		conf: c,
		ctx:  ctx,
	}
}

func (k *kafkaMessageDispatcher) GetProcessedMessageAndDestinationTopic(m []byte) ([]byte, string, error) {
	var message map[string]interface{}
	defer func() { k.DestinationTopic = "" }()
	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &message)
	if err != nil {
		return nil, "", err
	}
	k.EventId, err = utils.ExtractStringField(k.log, m, constants.EVENT_ID)
	if err != nil {
		return nil, "", err
	}
	appId, err := utils.ExtractStringField(k.log, m, constants.APP_ID)
	if err != nil {
		return nil, "", err
	}
	k.AppId = utils.GetServiceName(appId)
	err = k.setRetryFieldsAndDestinationTopic(&message)
	if err != nil {
		return nil, "", err
	}
	err = utils.SetDefaultFields(&message)
	if err != nil {
		return nil, "", err
	}
	// Convert map to JSON bytes
	jsonByteMessage, err := json.Marshal(&message)
	if err != nil {
		return nil, "", err
	}
	k.SourceTopic, err = utils.ExtractRetryMetaStringField(k.log, m, constants.ORIGINAL_TOPIC)
	if err != nil {
		return nil, "", err
	}
	err = k.checkRetryCounterValidity()
	if err != nil {
		return nil, "", err
	}
	return jsonByteMessage, k.DestinationTopic, nil
}

// setta i valori del counter e del nextRetryAttempt, e si calcola il destination topic
func (k *kafkaMessageDispatcher) setRetryFieldsAndDestinationTopic(oMessage *map[string]interface{}) error {
	retryMeta, ok := (*oMessage)[constants.RETRY_OBJECT_NAME].(map[string]interface{})
	if ok {

		retryCounterValue := k.getRetryCounterValue(&retryMeta)
		if retryCounterValue == -1 {
			return errors.New("null or invalid type value for retry_counter")
		}
		retryMeta[constants.RETRY_COUNTER] = retryCounterValue

		k.DestinationTopic = k.RetrieveDestinationTopic(int(retryCounterValue))

		nextRetryAttempt := k.getNextRetryAttempt(k.DestinationTopic)
		retryMeta[constants.NEXT_ATTEMPT] = nextRetryAttempt.Format("2006-01-02 15:04:05 -0700 MST")
		k.RetryCounterValue = retryCounterValue
		k.NextRetryAttempt = utils.GetRomeTimeZoneDateTime(nextRetryAttempt)

		//updating original message with new values
		(*oMessage)[constants.RETRY_OBJECT_NAME] = retryMeta
		return nil
	}
	return errors.New("failed to set retry field or destination topic")
}

func (k *kafkaMessageDispatcher) getRetryCounterValue(retryMeta *map[string]interface{}) float64 {
	if retryMeta == nil {
		k.log.Debug("DISPATCHER ->retry meta object can't be null")
		return -1
	}
	k.log.Debug(fmt.Sprintf("DISPATCHER -> retry meta content: %+v", *retryMeta))
	value, ok := (*retryMeta)[constants.RETRY_COUNTER]
	if ok {

		switch i := value.(type) {
		case float64:
			return i + 1
		default:
			return -1
		}
	}
	return 1
}

func (k *kafkaMessageDispatcher) checkRetryCounterValidity() error {

	cache := k.conf.Cache.CounterCache
	currentEventId := k.EventId
	currentCounter := k.RetryCounterValue
	//fmt.Println("currentEventId: " + currentEventId + ", currentCounter: " + strconv.Itoa(int(currentCounter)))
	counterData, found := cache.Get(k.ctx, currentEventId)
	if found != nil {
		//fmt.Println(" l'evento è nuovo!")
		k.log.Debug("event not stored in memory: Added.")
		// The event is not in the cache, initialize it with a counter of 1
		newCounter := config.CacheCounter{
			EventId:      currentEventId,
			RetryCounter: 1.0,
		}
		cache.Set(k.ctx, newCounter.EventId, &newCounter)
		return nil
	}
	k.log.Debug("known event: previous retry_counter vales was " + strconv.Itoa(int(counterData.RetryCounter)) + " and the current one is: " + strconv.Itoa(int(currentCounter)))
	//fmt.Println(" l'evento è già passato!! con retry counter: " + strconv.Itoa(int(counterData.RetryCounter)))
	if counterData.RetryCounter >= currentCounter {
		k.log.Debug("event already handled, but retry_counter is not right: previous was " + strconv.Itoa(int(counterData.RetryCounter)) + " and the current one is: " + strconv.Itoa(int(currentCounter)))
		return errors.New("event already handled, but retry_counter is not right: previous was " + strconv.Itoa(int(counterData.RetryCounter)) + " and the current one is: " + strconv.Itoa(int(currentCounter)))
	}
	cache.Set(k.ctx, currentEventId, &config.CacheCounter{
		EventId:      currentEventId,
		RetryCounter: currentCounter,
	})
	return nil
}

func (k *kafkaMessageDispatcher) RetrieveDestinationTopic(retryCounter int) string {
	tot := 0
	minRange := 0
	maxRange := 0

	for index, topic := range k.conf.Kafka.RetryTopicsList {
		maxRange = tot + k.conf.Policy.TopicRetryAttempts[index]
		minRange = tot + 1
		if retryCounter >= minRange && retryCounter <= maxRange {

			return topic
		}
		tot = tot + k.conf.Policy.TopicRetryAttempts[index]
	}
	return k.conf.Kafka.DLQTopic
}

func (k *kafkaMessageDispatcher) getNextRetryAttempt(destinationTopic string) time.Time {
	currentTime := time.Now()
	return currentTime.Add(time.Duration(k.conf.Policy.TopicSleepTimeMap[destinationTopic]) * time.Minute)
}

func (k *kafkaMessageDispatcher) GetDestinationTopic() string {
	return k.DestinationTopic
}

func (k *kafkaMessageDispatcher) GetSourceTopic() string {
	return k.SourceTopic
}

func (k *kafkaMessageDispatcher) GetEventId() string {
	return k.EventId
}

func (k *kafkaMessageDispatcher) GetRetryCounter() float64 {
	return k.RetryCounterValue
}

func (k *kafkaMessageDispatcher) GetNextRetryAttempt() string {
	return k.NextRetryAttempt
}

func (k *kafkaMessageDispatcher) GetAppId() string {
	return k.AppId
}
