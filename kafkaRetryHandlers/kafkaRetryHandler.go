package kafkaretryhandlers

import (
	"encoding/json"
	"math"
	"time"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/retryOrchestrator/config"
	"opencitylabs.it/retryOrchestrator/constants"
	"opencitylabs.it/retryOrchestrator/sentryutils"
	"opencitylabs.it/retryOrchestrator/utils"
)

type IKafkaRetryHandler interface {
	SleepAndGetDestinationTopic(m []byte) (string, error)
	waitForNextAttempt(duration float64)
	SetRetryHandlerName(topicName string)
	isTheNextRetryAttemptTimeExpired(minutesGap int) bool
	GetEventId() string
	GetRetryCounter() float64
	GetNextRetryAttempt() string
	GetAppId() string
}

type kafkaRetryHandler struct {
	log               *zap.Logger
	conf              config.Config
	sentryHub         *sentry.Hub
	handlerName       string
	DestinationTopic  string
	RetryCounterValue float64
	EventId           string
	NextRetryAttempt  string
	AppId             string
}

func NewKafkaRetryHandler(l *zap.Logger, c config.Config) IKafkaRetryHandler {
	return &kafkaRetryHandler{
		log:         l,
		conf:        c,
		sentryHub:   sentryutils.InitLocalSentryhub(),
		handlerName: "",
	}
}

func (k *kafkaRetryHandler) SleepAndGetDestinationTopic(m []byte) (string, error) {
	// Create an empty interface to unmarshal the message into a map
	var data map[string]interface{}

	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &data)
	if err != nil {
		return "", err
	}
	k.DestinationTopic, err = utils.ExtractRetryMetaStringField(k.log, m, constants.ORIGINAL_TOPIC)
	if err != nil {
		return "", err
	}
	nextRetryAttemptString, err := utils.ExtractRetryMetaStringField(k.log, m, constants.NEXT_ATTEMPT)
	if err != nil {
		return "", err
	}

	nextRetryAttempt, err := time.Parse(constants.DATA_TIME_LAYOUT, nextRetryAttemptString)
	if err != nil {
		return "", err
	}
	duration := time.Since(nextRetryAttempt).Minutes()

	if k.isTheNextRetryAttemptTimeExpired(int(duration)) {
		return k.DestinationTopic, nil
	} else {
		k.waitForNextAttempt(duration)
	}

	k.EventId, err = utils.ExtractStringField(k.log, m, constants.EVENT_ID)
	if err != nil {
		return "", err
	}
	appId, err := utils.ExtractStringField(k.log, m, constants.APP_ID)
	if err != nil {
		return "", err
	}
	k.AppId = utils.GetServiceName(appId)
	k.RetryCounterValue, err = utils.GetRetryCounterValue(m)
	k.RetryCounterValue = k.RetryCounterValue - 1
	if err != nil {
		return "", err
	}
	k.NextRetryAttempt = utils.GetRomeTimeZoneDateTime(nextRetryAttempt)
	return k.DestinationTopic, nil
}

func (k *kafkaRetryHandler) isTheNextRetryAttemptTimeExpired(minutesGap int) bool {
	return minutesGap > 0
}

func (k *kafkaRetryHandler) waitForNextAttempt(duration float64) {
	absoluteMinutesToWait := math.Abs(duration) + 1
	sleeptime := time.Duration(absoluteMinutesToWait) * time.Minute
	time.Sleep(sleeptime)
}

func (k *kafkaRetryHandler) SetRetryHandlerName(topicName string) {
	k.handlerName = topicName
}

func (k *kafkaRetryHandler) GetEventId() string {
	return k.EventId
}

func (k *kafkaRetryHandler) GetRetryCounter() float64 {
	return k.RetryCounterValue
}

func (k *kafkaRetryHandler) GetNextRetryAttempt() string {
	return k.NextRetryAttempt
}

func (k *kafkaRetryHandler) GetAppId() string {
	return k.AppId
}
