package kafkaretryhandlers

import (
	"testing"

	"go.uber.org/zap"
	"opencitylabs.it/retryOrchestrator/config"
	"opencitylabs.it/retryOrchestrator/logger"
)

func setup(t *testing.T) (*zap.Logger, *config.Config) {

	logger, err := logger.GetLoggerConfig().Build()
	if err != nil {
		logger = zap.NewNop()
	}
	defer logger.Sync()

	config := config.NewConfig(logger)
	return logger, config
}

func TestIsTheNextRetryAttemptTimeExpired(t *testing.T) {
	// Test when minutesGap is greater than 0
	minutesGap := 5
	logger, config := setup(t)
	retryHandler := NewKafkaRetryHandler(logger, *config)
	result := retryHandler.isTheNextRetryAttemptTimeExpired(minutesGap)
	if !result {
		t.Errorf("Expected true, got false")
	}

	// Test when minutesGap is 0
	minutesGap = 0
	result = retryHandler.isTheNextRetryAttemptTimeExpired(minutesGap)
	if result {
		t.Errorf("Expected false, got true")
	}

	// Test when minutesGap is negative
	minutesGap = -5
	result = retryHandler.isTheNextRetryAttemptTimeExpired(minutesGap)
	if result {
		t.Errorf("Expected false, got true")
	}
}
