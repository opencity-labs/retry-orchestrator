package logger

import (
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func GetLoggerConfig() zap.Config {
	encoderConfig := zapcore.EncoderConfig{
		MessageKey:     "message",
		LevelKey:       "level",
		TimeKey:        "time",
		CallerKey:      "caller",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.CapitalLevelEncoder,
		EncodeTime:     CustomTimeEncoder, // Use custom time encoder
		EncodeDuration: zapcore.StringDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	// Create the Zap logger with the Apache log format.
	config := zap.Config{
		Level:             zap.NewAtomicLevelAt(zapcore.DebugLevel),
		Encoding:          "json",
		EncoderConfig:     encoderConfig,
		OutputPaths:       []string{"stdout"},
		ErrorOutputPaths:  []string{"stderr"},
		DisableStacktrace: false,
	}
	return config
}

// CustomTimeEncoder is a custom time encoder that adds 2 hours to the log time.
func CustomTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	// Add 2 hours to the log time
	t = t.Add(2 * time.Hour)
	// Encode the time in ISO8601 format
	enc.AppendString(t.Format(time.RFC3339))
}
