package main

import (
	"context"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/retryOrchestrator/config"
	httpserver "opencitylabs.it/retryOrchestrator/httpServer"
	kafkaconsumer "opencitylabs.it/retryOrchestrator/kafkaIO/kafkaConsumer"
	kafkamessagedispatcher "opencitylabs.it/retryOrchestrator/kafkaMessageDispatcher"
	kafkaretryhandlers "opencitylabs.it/retryOrchestrator/kafkaRetryHandlers"
	"opencitylabs.it/retryOrchestrator/logger"
	"opencitylabs.it/retryOrchestrator/metrics"
	"opencitylabs.it/retryOrchestrator/sentryutils"
	"opencitylabs.it/retryOrchestrator/utils"
)

func main() {
	//logger init
	logger, err := logger.GetLoggerConfig().Build()
	if err != nil {
		logger = zap.NewNop()
	}
	defer logger.Sync()
	//config init
	config := config.NewConfig(logger)
	//sentry init
	err = sentryutils.SentryInit(config)
	if err != nil {
		logger.Error("sentry.Init: ", zap.Error(err))
		sentry.CaptureException(err)
	}

	ctx, exitFn := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	//kafka dispatcher consumer init
	dispatcher := kafkamessagedispatcher.NewKafkaMessageDispatcher(logger, *config, ctx)
	dispatcherKafkaConsumer := kafkaconsumer.NewKafkaConsumer(logger, config, config.Kafka.KafkaServer, config.Kafka.RetryQueueTopic, config.Kafka.ConsumerGroup)

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	wg.Add(2)
	go func() {
		defer wg.Done()
		dispatcherListener(ctx, logger, config, dispatcher, dispatcherKafkaConsumer)
	}()
	buildRetryhandlers(logger, config, ctx)

	//metrics and healthcheck
	server := httpserver.NewHttpServer(logger, *config)
	go func() {
		defer wg.Done()
		logger.Debug("server listening on " + config.Server.AddressPort)
		server.Run()
	}()

	// gracefully shutdown
	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	wg.Wait()
	exitFn()
	sentry.Flush(2 * time.Second)
	_ = logger.Sync()
	// a timeout of 15 seconds to shutdown the server
	_, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	dispatcherKafkaConsumer.CloseKafkaReader()

	logger.Debug("all processes done, shutting down")
}

func dispatcherListener(ctx context.Context, logger *zap.Logger, config *config.Config, dispatcher kafkamessagedispatcher.IKafkaMessageDispatcher, KafkaConsumer *kafkaconsumer.KafkaConsumer) {
	for {
		m, err := KafkaConsumer.KafkaReader.FetchMessage(ctx) // read without committing
		if err != nil {
			logger.Error("DISPATCHER -> error reading from"+config.Kafka.RetryQueueTopic+" topic: ", zap.Error(err))
			sentry.CaptureException(err)

			err = KafkaConsumer.KafkaReader.CommitMessages(ctx, m)
			if err != nil {
				logCommitError(logger, *config, dispatcher, err)
				sentry.CaptureException(err)
				commitMessage(logger, ctx, KafkaConsumer, m, "DISPATCHER", dispatcher.GetAppId())
				metrics.MetricsFailTotalDispatcherProcessed.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()

				continue
			}
			metrics.MetricsFailTotalDispatcherProcessed.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()

			continue
		}
		processedMessage, destinationTopic, err := dispatcher.GetProcessedMessageAndDestinationTopic(m.Value)
		if err != nil {
			//logErrorLite(logger, "failed get message or destination topic", *config, "dispatcher", err)
			logError(logger, "failed get message or destination topic", *config, dispatcher, "dispatcher", config.Kafka.DLQTopic, err)
			err = produceMessage(string(m.Key), m.Value, ctx, config.Kafka.KafkaServer, config.Kafka.DLQTopic)
			if err != nil {
				logError(logger, "failed get message or destination topic", *config, dispatcher, "dispatcher", config.Kafka.DLQTopic, err)
				sentry.CaptureException(err)

				err = KafkaConsumer.KafkaReader.CommitMessages(ctx, m)
				if err != nil {
					logCommitError(logger, *config, dispatcher, err)
					sentry.CaptureException(err)
					commitMessage(logger, ctx, KafkaConsumer, m, "DISPATCHER", dispatcher.GetAppId())
					metrics.MetricsFailTotalDispatcherProcessed.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()

					continue
				}
				commitMessage(logger, ctx, KafkaConsumer, m, "DISPATCHER", dispatcher.GetAppId())
				metrics.MetricsFailTotalDispatcherProcessed.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()

				continue
			}
			metrics.MetricsDeadLetterQueuemessage.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()
			sentry.CaptureException(err)
			commitMessage(logger, ctx, KafkaConsumer, m, "DISPATCHER", dispatcher.GetAppId())
			metrics.MetricsFailTotalDispatcherProcessed.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()

			continue
		} else {
			err = produceMessage(string(m.Key), processedMessage, ctx, config.Kafka.KafkaServer, destinationTopic)
			if err != nil {
				logError(logger, "failed to produce message", *config, dispatcher, "dispatcher", config.Kafka.DLQTopic, err)
				err = KafkaConsumer.KafkaReader.CommitMessages(ctx, m)
				if err != nil {
					logCommitError(logger, *config, dispatcher, err)
					sentry.CaptureException(err)
					commitMessage(logger, ctx, KafkaConsumer, m, "DISPATCHER", dispatcher.GetAppId())
					metrics.MetricsFailTotalDispatcherProcessed.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()

					continue
				}
				commitMessage(logger, ctx, KafkaConsumer, m, "DISPATCHER", dispatcher.GetAppId())
				metrics.MetricsFailTotalDispatcherProcessed.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()
				sentry.CaptureException(err)
				continue
			}
		}
		metrics.OpsSuccessDispatcherProcessed.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()
		logInfo(logger, "OK", *config, dispatcher, "dispatcher", destinationTopic)
		if destinationTopic == config.Kafka.DLQTopic {
			metrics.MetricsDeadLetterQueuemessage.WithLabelValues(config.App.Environment, dispatcher.GetAppId()).Inc()
		}
		commitMessage(logger, ctx, KafkaConsumer, m, "DISPATCHER", dispatcher.GetAppId())
	}

}

func retryHandlerListener(ctx context.Context, logger *zap.Logger, config *config.Config, retryHandler kafkaretryhandlers.IKafkaRetryHandler, KafkaConsumer *kafkaconsumer.KafkaConsumer, consumerTopic string) {

	logger.Sugar().Debug("building retry handler: ", consumerTopic)
	retryHandler.SetRetryHandlerName(consumerTopic)
	defer KafkaConsumer.CloseKafkaReader()
	for {
		m, err := KafkaConsumer.KafkaReader.FetchMessage(ctx) // read without committing
		if err != nil {
			logger.Error("RETRY_HANDLER "+consumerTopic+" -> error reading from: "+consumerTopic+" topic: ", zap.Error(err))
			sentry.CaptureException(err)
			commitMessage(logger, ctx, KafkaConsumer, m, "RETRY_HANDLER "+consumerTopic, retryHandler.GetAppId())
			metrics.OpsTotalFailHandlerProcessed.WithLabelValues(config.App.Environment, consumerTopic, retryHandler.GetAppId()).Inc()
			continue
		}
		destinationTopic, err := retryHandler.SleepAndGetDestinationTopic(m.Value)
		if err != nil {
			logger.Info("RETRY_HANDLER " + consumerTopic + " ->, result: KO, ENV: " + config.App.Environment + ", event id: " + retryHandler.GetEventId() + ", from topic: " + destinationTopic + ", attempt number: " + strconv.FormatFloat(retryHandler.GetRetryCounter(), 'f', -1, 64) + ", next attempt at: " + retryHandler.GetNextRetryAttempt() + ", error: " + err.Error())
			sentry.CaptureException(err)

			err = produceMessage(string(m.Key), m.Value, ctx, config.Kafka.KafkaServer, config.Kafka.DLQTopic)
			if err != nil {
				logger.Sugar().Error("RETRY_HANDLER "+consumerTopic+" -> message key: ", string(m.Key)+", failed to produce event message to topic: "+config.Kafka.DLQTopic)
				sentry.CaptureException(err)
				commitMessage(logger, ctx, KafkaConsumer, m, "RETRY_HANDLER "+consumerTopic, retryHandler.GetAppId())
				metrics.OpsTotalFailHandlerProcessed.WithLabelValues(config.App.Environment, consumerTopic, retryHandler.GetAppId()).Inc()
				continue
			}
			logger.Info("RETRY_HANDLER " + consumerTopic + " result: failed to process message, added to topic " + config.Kafka.DLQTopic)
			metrics.MetricsDeadLetterQueuemessage.WithLabelValues(config.App.Environment, retryHandler.GetAppId()).Inc()
			sentry.CaptureException(err)
			commitMessage(logger, ctx, KafkaConsumer, m, "RETRY_HANDLER "+consumerTopic, retryHandler.GetAppId())
			metrics.OpsTotalFailHandlerProcessed.WithLabelValues(config.App.Environment, consumerTopic, retryHandler.GetAppId()).Inc()
			continue
		}
		message, err := utils.ParseKafkaMessageToMap(m.Value)
		if err != nil {
			logger.Sugar().Error("RETRY_HANDLER "+consumerTopic+" -> message key: ", string(m.Key)+", failed to parse event to map")
			sentry.CaptureException(err)
			commitMessage(logger, ctx, KafkaConsumer, m, "RETRY_HANDLER "+consumerTopic, retryHandler.GetAppId())
			metrics.OpsTotalFailHandlerProcessed.WithLabelValues(config.App.Environment, consumerTopic, retryHandler.GetAppId()).Inc()
			continue
		}
		err = utils.SetDefaultFields(&message)
		if err != nil {
			logger.Sugar().Error("RETRY_HANDLER "+consumerTopic+" -> message key: ", string(m.Key)+", error: "+err.Error())
			sentry.CaptureException(err)
			commitMessage(logger, ctx, KafkaConsumer, m, "RETRY_HANDLER "+consumerTopic, retryHandler.GetAppId())
			metrics.OpsTotalFailHandlerProcessed.WithLabelValues(config.App.Environment, consumerTopic, retryHandler.GetAppId()).Inc()
			continue
		}
		kafkaMessageWithDefaultFields, err := utils.ConvertMapToKafkaEvent(&message)
		if err != nil {
			logger.Sugar().Error("RETRY_HANDLER "+consumerTopic+" -> message key: ", string(m.Key)+", failed to Convert Map To Kafka Event")
			sentry.CaptureException(err)
			commitMessage(logger, ctx, KafkaConsumer, m, "RETRY_HANDLER "+consumerTopic, retryHandler.GetAppId())
			metrics.OpsTotalFailHandlerProcessed.WithLabelValues(config.App.Environment, consumerTopic, retryHandler.GetAppId()).Inc()
			continue
		}
		err = produceMessage(string(m.Key), kafkaMessageWithDefaultFields, ctx, config.Kafka.KafkaServer, destinationTopic)
		if err != nil {
			logger.Sugar().Error("RETRY_HANDLER "+consumerTopic+" -> message key: ", string(m.Key)+", failed to produce the event on topic "+destinationTopic)
			sentry.CaptureException(err)
			commitMessage(logger, ctx, KafkaConsumer, m, "RETRY_HANDLER "+consumerTopic, retryHandler.GetAppId())
			metrics.OpsTotalFailHandlerProcessed.WithLabelValues(config.App.Environment, consumerTopic, retryHandler.GetAppId()).Inc()
			continue
		}
		logger.Info("RETRY_HANDLER " + consumerTopic + " ->, result: OK, ENV: " + config.App.Environment + ", event id: " + retryHandler.GetEventId() + ", produced to topic: " + destinationTopic + ", attempt number: " + strconv.FormatFloat(retryHandler.GetRetryCounter(), 'f', -1, 64))
		metrics.OpsSuccessHandlerProcessed.WithLabelValues(config.App.Environment, consumerTopic, retryHandler.GetAppId()).Inc()
		commitMessage(logger, ctx, KafkaConsumer, m, "RETRY_HANDLER "+consumerTopic, retryHandler.GetAppId())
	}

}

func produceMessage(key string, message []byte, ctx context.Context, kafkaServer []string, kafkaTopic string) error {
	KafkaWriter := getKafkaWriter(kafkaServer, kafkaTopic)
	err := KafkaWriter.WriteMessages(ctx, kafka.Message{
		Key:   []byte(key),
		Value: message,
	})
	if err != nil {
		return err
	}
	err = KafkaWriter.Close()
	if err != nil {
		return err
	}
	return nil
}

func getKafkaWriter(kafkaServer []string, kafkaTopic string) *kafka.Writer {
	transport := &kafka.DefaultTransport
	r := &kafka.Writer{
		Addr:      kafka.TCP(kafkaServer...),
		Topic:     kafkaTopic,
		Balancer:  &kafka.LeastBytes{},
		Transport: *transport,
	}
	return r
}

func buildRetryhandlers(logger *zap.Logger, config *config.Config, ctx context.Context) {

	for _, topic := range config.Kafka.RetryTopicsList {
		kafkaConsumer := kafkaconsumer.NewKafkaConsumer(logger, config, config.Kafka.KafkaServer, topic, config.Kafka.ConsumerGroup)

		retryHandler := kafkaretryhandlers.NewKafkaRetryHandler(logger, *config)
		go retryHandlerListener(ctx, logger, config, retryHandler, kafkaConsumer, topic)
	}
}

func commitMessage(logger *zap.Logger, ctx context.Context, KafkaConsumer *kafkaconsumer.KafkaConsumer, m kafka.Message, actor, appId string) {
	err := KafkaConsumer.KafkaReader.CommitMessages(ctx, m)
	if err != nil {
		logger.Error(actor+" -> message key: "+string(m.Key)+" failed to commit messages: ", zap.Error(err))
		sentry.CaptureException(err)
	}
}

func logCommitError(logger *zap.Logger, config config.Config, dispatcher kafkamessagedispatcher.IKafkaMessageDispatcher, err error) {
	logger.Error("Failed to commit message",
		zap.String("actor", "DISPATCHER"),
		zap.String("result", "failed"),
		zap.String("error", err.Error()),
		zap.String("environment", config.App.Environment),
		zap.String("event_id", dispatcher.GetEventId()),
		zap.String("from_topic", dispatcher.GetSourceTopic()),
		zap.String("from_service", dispatcher.GetAppId()),
		zap.String("attempt_number", strconv.FormatFloat(dispatcher.GetRetryCounter(), 'f', -1, 64)),
		zap.String("next_attempt_at", dispatcher.GetNextRetryAttempt()),
	)
}

func logError(logger *zap.Logger, message string, config config.Config, dispatcher kafkamessagedispatcher.IKafkaMessageDispatcher, actor string, destinationTopic string, err error) {
	logger.Error(message,
		zap.String("actor", actor),
		zap.String("result", "failed"),
		zap.String("destination_topic", config.Kafka.DLQTopic),
		zap.String("error", err.Error()),
		zap.String("environment", config.App.Environment),
		zap.String("event_id", dispatcher.GetEventId()),
		zap.String("from_service", dispatcher.GetAppId()),
		zap.String("from_topic", dispatcher.GetSourceTopic()),
		zap.String("produced_to_topic", destinationTopic),
		zap.String("attempt_number", strconv.FormatFloat(dispatcher.GetRetryCounter(), 'f', -1, 64)),
		zap.String("next_attempt_at", dispatcher.GetNextRetryAttempt()),
	)
}
func logErrorLite(logger *zap.Logger, message string, config config.Config, actor string, err error) {
	logger.Error(message,
		zap.String("actor", actor),
		zap.String("result", "failed"),
		zap.String("destination_topic", config.Kafka.DLQTopic),
		zap.String("error", err.Error()),
		zap.String("environment", config.App.Environment),
	)
}

func logInfo(logger *zap.Logger, message string, config config.Config, dispatcher kafkamessagedispatcher.IKafkaMessageDispatcher, actor string, destinationTopic string) {
	logger.Info(message,
		zap.String("actor", actor),
		zap.String("environment", config.App.Environment),
		zap.String("event_id", dispatcher.GetEventId()),
		zap.String("from_service", dispatcher.GetAppId()),
		zap.String("from_topic", dispatcher.GetSourceTopic()),
		zap.String("produced_to_topic", destinationTopic),
		zap.String("attempt_number", strconv.FormatFloat(dispatcher.GetRetryCounter(), 'f', -1, 64)),
		zap.String("next_attempt_at", dispatcher.GetNextRetryAttempt()),
	)
}
