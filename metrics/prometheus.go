package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	OpsSuccessDispatcherProcessed = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_retry_orchestrator_success_event_processed_by_dispatcher_total",
		Help: "The total number of successfully processed events by dispatcher ordered by producer service",
	}, []string{"env", "app_name"})

	MetricsFailTotalDispatcherProcessed = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_retry_orchestrator_failed_event_Processed_by_dispatcher_total",
		Help: "The total number of failed processed events by dispatcher ordered by producer service",
	}, []string{"env", "app_name"})

	OpsSuccessHandlerProcessed = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_retry_orchestrator_success_event_Processed_by_handlers_total",
		Help: "The total number of successfully processed events ordered by producer service and handlers",
	}, []string{"env", "handler", "app_name"})

	OpsTotalFailHandlerProcessed = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_retry_orchestrator_failed_event_Processed_by_handlers_total",
		Help: "The total number of failed processed events ordered by producer service and handlers",
	}, []string{"env", "handler", "app_name"})

	MetricsDeadLetterQueuemessage = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_retry_orchestrator_dlq_event_total",
		Help: "The total number of event produced to dead letter queue ordered by producer service",
	}, []string{"env", "app_name"})
)
