#!/bin/bash

# Sleep for 10 seconds to allow services to start
sleep 10
# Wait for Kafka to be ready
wait-for-it kafka:9092

# Configure Kafka cluster settings
kaf config add-cluster local -b kafka:9092 && \
kaf config use-cluster local

# Create Kafka topics with specified partitions and replicas
kaf topic create --partitions 1 --replicas 1 retryQueue || true
kaf topic create --partitions 1 --replicas 1 retryQueue_1m || true
kaf topic create --partitions 1 --replicas 1 retryQueue_2m || true
kaf topic create --partitions 1 --replicas 1 retryQueue_3m || true
kaf topic create --partitions 1 --replicas 1 retryDLQ || true
kaf topic create --partitions 1 --replicas 1 documents || true

# Produce messages to the retryQueue topic using data from /data/retryQueue.json
cat ./test/retryQueue.json | jq -c . | kaf produce retryQueue

echo "12345 retry orchestrator starting"
#running the service
./retry-orchestrator &
echo "12345 retry orchestrator started"

# Send HTTP request to get metrics and retrieve response body
metrics_response=$(curl -s http://0.0.0.0:9090/metrics)

# Extract the value of oc_retry_orchestrator_success_event_processed_by_dispatcher_total
oc_retry_orchestrator_success=$(echo "$metrics_response" | grep -oE 'oc_retry_orchestrator_success_event_processed_by_dispatcher_total [0-9]+')

# Check if the value is equal to 1
if [[ $oc_retry_orchestrator_success =~ [0-9]+$ ]]; then
    if [ "${BASH_REMATCH[0]}" -ne 1 ]; then
        echo "Error: oc_retry_orchestrator_success_event_processed_by_dispatcher_total is not equal to 1"
        exit 1
    fi
else
    echo "Error: Unable to extract oc_retry_orchestrator_success_event_processed_by_dispatcher_total value"
    exit 1
fi


