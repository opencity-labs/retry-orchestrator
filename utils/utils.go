package utils

import (
	"encoding/json"
	"errors"
	"strings"
	"time"

	"github.com/google/uuid"
	"go.uber.org/zap"
	"opencitylabs.it/retryOrchestrator/config"
	"opencitylabs.it/retryOrchestrator/constants"
)

func IsStringFieldFound(m map[string]interface{}, fieldToFind string) bool {

	_, found := m[fieldToFind].(string)
	return found
}

func ExtractRetryMetaStringField(log *zap.Logger, m []byte, fieldToExtract string) (string, error) {
	// Create an empty interface to unmarshal the message into a map
	var data map[string]interface{}
	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &data)
	if err != nil {
		return "", errors.New("error extracting field " + fieldToExtract + ": " + err.Error())
	}
	//check the original topic
	retryMeta, ok := data[constants.RETRY_OBJECT_NAME].(map[string]interface{})
	if ok && IsStringFieldFound(retryMeta, fieldToExtract) {
		//log.Sugar().Debug(" field %s current value : %s ", fieldToExtract, retryMeta[fieldToExtract])
		return retryMeta[fieldToExtract].(string), nil
	}
	return "", errors.New("error extracting field " + fieldToExtract + ": field not found ")
}
func ExtractStringField(log *zap.Logger, m []byte, fieldToExtract string) (string, error) {
	// Create an empty interface to unmarshal the message into a map
	var data map[string]interface{}
	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &data)
	if err != nil {
		return "", errors.New("error extracting field " + fieldToExtract + ": " + err.Error())
	}

	// Check if the field "id" exists
	id, ok := data[fieldToExtract].(string)
	if ok {
		return id, nil
	}

	// If the "id" field does not exist, return an error
	return "", errors.New("error extracting field " + fieldToExtract + ": field not found")
}

func SetDefaultFields(oMessage *map[string]interface{}) error {

	newAppId := "retry-orchestrator:" + config.VERSION
	_, ok := (*oMessage)["app_id"]
	if ok {
		(*oMessage)["app_id"] = newAppId
	} else {
		return errors.New("unable to set fild app_id")
	}
	newEventCreatedAt := GetRomeTimeZoneDateTime(time.Now().UTC())

	_, ok = (*oMessage)["event_created_at"]
	if ok {
		//updating original message with new values
		(*oMessage)["event_created_at"] = newEventCreatedAt
	} else {
		return errors.New("unable to set fild event_created_at")
	}

	_, ok = (*oMessage)["unable to set field updated_at"]
	if ok {
		//updating original message with new values
		(*oMessage)["updated_at"] = newEventCreatedAt
	} /*  else {
		return errors.New("unable to set field updated_at")
	} */

	newUuidEventId := uuid.NewString()
	_, ok = (*oMessage)["event_id"]
	if ok {
		//updating original message with new values
		(*oMessage)["event_id"] = newUuidEventId
	} else {
		return errors.New("unable to set fild event_id")
	}
	return nil
}

func ConvertMapToKafkaEvent(iMessage *map[string]interface{}) ([]byte, error) {
	jsonByteMessage, err := json.Marshal(&iMessage)
	if err != nil {
		return nil, err
	}
	return jsonByteMessage, nil
}

func ParseKafkaMessageToMap(m []byte) (map[string]interface{}, error) {
	var message map[string]interface{}
	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &message)
	if err != nil {
		return nil, err
	}
	return message, nil

}

// juut to clean loop garbage messages.
func ShouldSkipThisEventVersion(m []byte, log *zap.Logger, version int) (bool, error) {
	// Create an empty interface to unmarshal the message into a map
	var event map[string]interface{}
	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &event)
	if err != nil {
		return false, err
	}
	// Use type assertion to convert the value to int
	eventVersion, ok := event["event_version"].(float64)
	if !ok {
		return false, errors.New("event_version is not a valid float64")
	}

	// Convert the float64 to int
	eventVersionInt := int(eventVersion)
	result := eventVersionInt == version
	return result, nil
}

func GetRomeTimeZoneDateTime(currentTimeUTC time.Time) string {
	romeLocation := time.FixedZone("Rome", 2*60*60)
	currentTimeRome := currentTimeUTC.In(romeLocation)
	iso8601Format := "2006-01-02T15:04:05+01:00"
	return currentTimeRome.Format(iso8601Format)
}

func GetRetryCounterValue(m []byte) (float64, error) {

	var message map[string]interface{}

	err := json.Unmarshal(m, &message)
	if err != nil {
		return -1, err
	}
	retryMeta, ok := (message)[constants.RETRY_OBJECT_NAME].(map[string]interface{})
	if ok {
		value, ok := (retryMeta)[constants.RETRY_COUNTER]
		if ok {
			switch i := value.(type) {
			case float64:
				return i + 1, nil
			default:
				return -1, errors.New("retry_counter is not float")
			}
		}
	}
	return -1, errors.New("retry_counter extrating failed")
}

func GetServiceName(input string) string {
	parts := strings.Split(input, ":")
	if len(parts) > 0 {
		return parts[0]
	}
	return ""
}

/*
func ExtractRetryMetaFloatField(log *zap.Logger, m []byte, fieldToExtract string) (float64, error) {
	// Create an empty interface to unmarshal the message into a map
	var data map[string]interface{}
	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &data)
	if err != nil {
		log.Error("error unmarshalling the message: ", zap.Error(err))
		return -1, err
	}
	//check the original topic
	retryMeta, ok := data[constants.RETRY_OBJECT_NAME].(map[string]interface{})
	if ok && IsRetryMetaFloatFieldFound(retryMeta, fieldToExtract) {
		log.Sugar().Debug(" field ", fieldToExtract, ": current value : ", retryMeta[fieldToExtract])
		return retryMeta[fieldToExtract].(float64), nil
	}
	return -1, err
}  */

/* func SetRetryMetaFloatValue(log *zap.Logger, m []byte, fieldToSet string, newValue float64) ([]byte, error) {
	// Create an empty interface to unmarshal the message into a map
	var data map[string]interface{}

	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &data)
	if err != nil {
		return nil, err
	}
	// Modify the values of original_topic and retry_policy
	retryMeta, ok := data[constants.RETRY_OBJECT_NAME].(map[string]interface{})
	if ok {
		log.Sugar().Debug("parsed retry_meta-retry_counter current value : ", retryMeta["retry_counter"])
		retryMeta["retry_counter"] = newValue
	}
	log.Sugar().Debug("parsed retry_meta-retry_counter modified value : ", retryMeta["retry_counter"])
	// Marshal the modified map back to JSON
	modifiedMessage, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	log.Sugar().Debug("full modified message : ", string(modifiedMessage))
	return modifiedMessage, nil

} */

/* func getMarshalledMessage(m []byte) (map[string]interface{}, error) {
	// Create an empty interface to unmarshal the message into a map
	var data map[string]interface{}
	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &data)
	if err != nil {
		return data, errors.New("error unmarshalling the message into a map")
	}

	return data, nil
} */

/* func IsRetryMetaFloatFieldFound(m map[string]interface{}, fieldToFind string) bool {

	_, found := m[fieldToFind].(float64)
	return found
} */
