package utils

import (
	"testing"

	"go.uber.org/zap"
)

func TestIsRetryMetaStringFieldFound(t *testing.T) {
	// Test case 1: Field found in map as string
	m := map[string]interface{}{
		"field1": "value1",
		"field2": 123,
	}
	fieldToFind := "field1"
	expected := true

	result := IsStringFieldFound(m, fieldToFind)

	if result != expected {
		t.Errorf("Field %s was expected to be found, but it was not found", fieldToFind)
	}

	// Test case 2: Field not found in map
	fieldToFind = "field3"
	expected = false

	result = IsStringFieldFound(m, fieldToFind)

	if result != expected {
		t.Errorf("Field %s was not expected to be found, but it was found", fieldToFind)
	}

	// Test case 3: Field found in map but not as string
	m = map[string]interface{}{
		"field1": 123,
		"field2": true,
	}
	fieldToFind = "field1"
	expected = false

	result = IsStringFieldFound(m, fieldToFind)

	if result != expected {
		t.Errorf("Field %s was not expected to be found as string, but it was found", fieldToFind)
	}
}

func TestExtractRetryMetaStringField(t *testing.T) {
	// Test case 1: Field found in retryMeta map as string
	log, _ := zap.NewDevelopment()
	m := []byte(`{"retry_meta": {"retry_counter": 2, "next_attempt_at": "2006-01-02 15:04:05.999999999 -0700 MST"}}`)
	fieldToExtract := "next_attempt_at"
	expected := "2006-01-02 15:04:05.999999999 -0700 MST"

	result, _ := ExtractRetryMetaStringField(log, m, fieldToExtract)

	if result != expected {
		t.Errorf("Field %s was expected to have value '%s', but got '%s'", fieldToExtract, expected, result)
	}

	// Test case 2: Field not found in retryMeta map
	m = []byte(`{"retry_meta": {"retry_counter": 2, "next_attempt_at": "2006-01-02 15:04:05.999999999 -0700 MST"}}`)
	fieldToExtract = "field1"
	expected = "error extracting field " + fieldToExtract + ": field not found "

	_, err := ExtractRetryMetaStringField(log, m, fieldToExtract)
	if err == nil {
		t.Errorf("expected error")
	}

	if err.Error() != expected {
		t.Errorf("Error %s is not '%s'", err.Error(), expected)
	}

	// Test case 3: Invalid JSON message
	m = []byte(`{"retry_meta": {"field1": "value1", "field2": 123}`)
	fieldToExtract = "field1"
	expected = ""

	result, _ = ExtractRetryMetaStringField(log, m, fieldToExtract)

	if result != expected {
		t.Errorf("Field %s was expected to be empty due to unmarshalling error, but got '%s'", fieldToExtract, result)
	}

	// Test case 4: Field found in retryMeta map but not as string
	m = []byte(`{"retry_meta": {"retry_counter": 2, "next_attempt_at": "2006-01-02 15:04:05.999999999 -0700 MST"}}`)
	fieldToExtract = "retry_counter"
	expected = ""

	result, _ = ExtractRetryMetaStringField(log, m, fieldToExtract)

	if result != expected {
		t.Errorf("Field %s was expected to be empty as it is not a string, but got '%s'", fieldToExtract, result)
	}
}

func TestExtractStringField(t *testing.T) {
	// Test case 1: Field found in retryMeta map as string
	log, _ := zap.NewDevelopment()
	m := []byte(`{"AccountId":"0015E00003AcX8nQAF","Categoria_del_Cittadino__r":{"Name":"Anagrafe, elettorale e cimiteri"},"ContactId":"123","Data_Apertura_Case__c":"2023-05-12T12:13:09+02:00","Description":"www2","id":"dasdf4464s"}`)
	fieldToExtract := "id"
	expected := "dasdf4464s"

	result, _ := ExtractStringField(log, m, fieldToExtract)

	if result != expected {
		t.Errorf("Field %s was expected to have value '%s', but got '%s'", fieldToExtract, expected, result)
	}

	// Test case 2: Field not found in retryMeta map
	m = []byte(`{"retry_meta": {"retry_counter": 2, "next_attempt_at": "2006-01-02 15:04:05.999999999 -0700 MST"}}`)
	fieldToExtract = "id"
	expected = "error extracting field " + fieldToExtract + ": field not found"

	_, err := ExtractStringField(log, m, fieldToExtract)
	if err == nil {
		t.Errorf("expected error")
	}

	if err.Error() != expected {
		t.Errorf("Error \"%s\" is not \"'%s'\"", err.Error(), expected)
	}

	// Test case 3: Invalid JSON message
	m = []byte(`{"retry_meta": {"field1": "value1", "field2": 123}`)
	fieldToExtract = "id"
	expected = ""

	result, _ = ExtractStringField(log, m, fieldToExtract)

	if result != expected {
		t.Errorf("Field %s was expected to be empty due to unmarshalling error, but got '%s'", fieldToExtract, result)
	}

	// Test case 4: Field found in map but not as string
	m = []byte(`{id: 34,"retry_meta": {"retry_counter": 2, "next_attempt_at": "2006-01-02 15:04:05.999999999 -0700 MST"}}`)
	fieldToExtract = "retry_counter"
	expected = ""

	result, _ = ExtractStringField(log, m, fieldToExtract)

	if result != expected {
		t.Errorf("Field %s was expected to be empty as it is not a string, but got '%s'", fieldToExtract, result)
	}

	// Test case 4: Field found in map but not as string
	m = []byte(`{"app_id": "test:234.324","id": "12345678","retry_meta": {"retry_counter": 2, "next_attempt_at": "2006-01-02 15:04:05.999999999 -0700 MST"}}`)
	fieldToExtract = "app_id"
	expected = "test:234.324"

	result, _ = ExtractStringField(log, m, fieldToExtract)

	if result != expected {
		t.Errorf("expected '%s' , but got '%s'", expected, result)
	}
	serviceNameExpected := "test"
	serviceNameResult := GetServiceName(result)
	if serviceNameExpected != serviceNameResult {
		t.Errorf("expected '%s' , but got '%s'", serviceNameExpected, serviceNameResult)
	}
}
